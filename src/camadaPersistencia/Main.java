package camadaPersistencia;

import camadaApresentacao.JanelaMenuGeral;
import camadaNegocios.Cliente;

public class Main {
	public static void povoandoArray() {
		Cliente p5 = new Cliente();
		p5.addP("Fulano", "Masculino", "Solteiro(a)", "18/05/1963", "29772695", "Estudante", "500", "Barcelona",
				"Rua C", "10", "Apto 201", "Centro", "97541-150", "Alegrete", "Rio Grande do Sul", "Do lado da padaria",
				"055 34221234", "055 99991234", "fulano@mail.com", "Fisica", "1000", "bla bla bla", "294.776.746-21",
				"");

		Cliente p2 = new Cliente();
		p2.addP("Ciclano", "Masculino", "Casado(a)", "22/06/1967", "41875789", "Professor", "1500", "Barcelona",
				"Rua B", "20", "Apto 201", "Centro", "97541-150", "Caxias do Sul", "Rio Grande do Sul",
				"Do lado da padaria", "055 34221234", "055 99991234", "Ciclano@mail.com", "Fisica", "1000",
				"bla bla bla", "122.816.613-77", "");

		Cliente p3 = new Cliente();
		p3.addP("Beltrana", "Feminino", "Divorciado(a)", "10/03/1969", "91122534", "Atendente", "2000", "Barcelona",
				"Rua A", "30", "Apto 201", "Centro", "97541-150", "Bage", "Rio Grande do Sul", "Do lado da padaria",
				"055 34221234", "055 99991234", "Beltrana@mail.com", "Fisica", "1000", "bla bla bla", "049.777.354-63",
				"");

		Cliente p4 = new Cliente();
		p4.addP("Furunculo", "Masculino", "Viuvo(a)", "20/07/1954", "40328944", "Programador", "800", "Barcelona",
				"Rua H", "40", "Apto 201", "Centro", "97541-150", "Rio Grande", "Rio Grande do Sul",
				"Do lado da padaria", "055 34221234", "055 99991234", "Furunculo@mail.com", "Fisica", "1000",
				"bla bla bla", "524.292.843-32", "");

		Cliente p = new Cliente();
		p.addP("Empresa ABC", "Feminino", "Separado(a)", "15/10/1969", "42943412", "Vendas automotivas", "6000",
				"Arsenal", "Rua G", "55", "Apto 14", "Centro", "97542-000", "Pelotas", "Rio Grande do Sul",
				"na frente do posto", "055 34224321", "055 99994321", "EmpresaABC@mail.com", "Juridica", "90000",
				"bla bla bla", "", "16.819.355/0001-47");
		Cliente p6 = new Cliente();
		p6.addP("Empresa DEF", "Masculino", "Companheiro(a)", "15/08/1991", "12341234", "Vendas alimenticias", "9000",
				"Arsenal", "Rua F", "35", "Apto 14", "Centro", "97542-000", "Uruguaiana", "Rio Grande do Sul",
				"na frente do posto", "055 34224321", "055 99994321", "EmpresaDEF@mail.com", "Juridica", "90000",
				"bla bla bla", "", "91.540.516/0001-84");

		Cliente p7 = new Cliente();
		p7.addP("Empresa GHI", "Feminino", "Solteiro(a)", "20/05/1987", "43214321", "Vendas de vestuario", "2500",
				"Arsenal", "Rua E", "15", "Apto 14", "Centro", "97542-000", "Porto Alegre", "Rio Grande do Sul",
				"na frente do posto", "055 34224321", "055 99994321", "EmpresaGHI@mail.com", "Juridica", "90000",
				"bla bla bla", "", "19.667.506/0001-87");

		Cliente p8 = new Cliente();
		p8.addP("Empresa JKL", "Masculino", "Casado(a)", "22/02/1975", "98769876", "Produtora de arroz", "3500",
				"Arsenal", "Rua D", "65", "Apto 14", "Centro", "97542-000", "Santa Maria", "Rio Grande do Sul",
				"na frente do posto", "055 34224321", "055 99994321", "EmpresaJKL@mail.com", "Juridica", "90000",
				"bla bla bla", "", "21.787.892/0001-92");
	}

	public static void main(String[] args) {
		povoandoArray();
		@SuppressWarnings("unused")
		JanelaMenuGeral mg = new JanelaMenuGeral();
	}

}