package camadaPersistencia;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import camadaNegocios.Cliente;

public class ArrayClientes {
	private static List<Cliente> clientes = new ArrayList<Cliente>();

	public static List<Cliente> getClientes() {
		return clientes;
	}

	public static void addCliente(Cliente p) {
		clientes.add(p);
		JOptionPane.showMessageDialog(null, " Cadastro Realizado ", "", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void atualizaCliente(int ID, Cliente p) {
		int linhaSelecionada = ID - 1;
		clientes.set(linhaSelecionada, p);
		JOptionPane.showMessageDialog(null, " Cadastro Atualizado ", "", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void removeCliente(Cliente p) {
		clientes.remove(p);
	}
}
