package camadaNegocios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @author Bernard 
 * @author Luis
 * @author Pedro
 * @author Rafael
 * @author Rodrigo
 */
public class ValidaClientes {

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui apenas letras aonde retorna true se achar algo na string que nao
	 * seja somente letras
	 */
	public static boolean validaLetras(String s) {
		// Define o padrao da string a ser obedecido
		Pattern pattern = Pattern.compile("[^A-Za-z ]");
		// Compara a string informada com o padrao definido
		Matcher matcher = pattern.matcher(s);
		// se a string passada possui algo que nao seja letras retorna true
		// se nao retorna falso
		if (matcher.find()) {
			return true;
		} else {
			return false;
		}
	}
	/*
	 * Metodo do tipo boolean para validar se a String informada nao possui
	 * valor nulo aonde retorna false se for nulo se nao retorna true Utilizado
	 * para validas campos obrigatorios
	 */

	public static boolean validaCampoNulo(String s) {
		if (s.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * equivale aos valores definidos para o campo Sexo. Se a String for igual a
	 * "masculino" ou "feminino" retorna true, caso contrario retorna false.
	 */

	public static boolean validaMasculinoFeminino(String s) {
		String m = "Masculino";
		String f = "Feminino";
		// verifica se "s" equivale a "masculino" ou "feminino"
		if (s.equals(m) || s.equals(f)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean validaEstadoCivilSelecionado(String s) {
		String n = "";
		String so = "Solteiro(a)";
		String c = "Casado(a)";
		String d = "Divorciado(a)";
		String v = "Viuvo(a)";
		String se = "Separado(a)";
		String co = "Companheiro(a)";
		// verifica se "s" equivale aos estados civis das strings
		if (s.equals(so) || s.equals(c) || s.equals(d) || s.equals(v) || s.equals(se) || s.equals(co) || s.equals(n)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean validaUFselecionado(String s) {
		String a = "Acre";
		String b = "Alagoas";
		String c = "Amazonas";
		String d = "Amap�";
		String e = "Bahia";
		String f = "Cear�";
		String g = "Distrito Federal";
		String h = "Esp�rito Santo";
		String i = "Goi�s";
		String j = "Maranh�o";
		String k = "Minas Gerais";
		String l = "Mato Grosso do Sul";
		String m = "Mato Grosso";
		String n = "Para�ba";
		String o = "Pernambuco";
		String p = "Piau�";
		String q = "Paran�";
		String r = "Rio de Janeiro";
		String s2 = "Rio Grande do Norte";
		String t = "Rond�nia";
		String u = "Roraima";
		String v = "Rio Grande do Sul";
		String x = "Santa Catarina";
		String y = "Sergipe";
		String z = "S�o Paulo";
		String a2 = "Tocantins";
		// verifica se "s" equivale aos estados civis das strings
		if (s.equals(a) || s.equals(b) || s.equals(c) || s.equals(d) || s.equals(e) || s.equals(f) || s.equals(g)
				|| s.equals(h) || s.equals(i) || s.equals(j) || s.equals(k) || s.equals(l) || s.equals(m) || s.equals(n)
				|| s.equals(o) || s.equals(p) || s.equals(q) || s.equals(r) || s.equals(s2) || s.equals(t)
				|| s.equals(u) || s.equals(v) || s.equals(x) || s.equals(y) || s.equals(z) || s.equals(a2)) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui apenas numeros e caracteres especiais. Retorna true se achar
	 * alguma letra na string, caso contrario retorna false.
	 */
	public static boolean validaNumerosCaracEspeciais(String s) {
		// Define o padrao da string a ser obedecido
		Pattern pattern = Pattern.compile("[A-Za-z]");
		// Compara a string informada com o padrao definido
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar a quantidade de caracteres na String
	 * informada como parametro Se possui tamanho superior a 50 caracteres
	 * retorna false, caso contrario retorna true.
	 */
	public static boolean validaMax50Carac(String s) {
		if (s.length() > 50) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar a quantidade de caracteres na String
	 * informada como parametro Se possui tamanho superior a 20 caracteres
	 * retorna false, caso contrario retorna true.
	 */
	public static boolean validaMax20Carac(String s) {
		if (s.length() > 20) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar a quantidade de caracteres na String
	 * informada como parametro Se possui tamanho superior a 300 caracteres
	 * retorna false, caso contrario retorna true.
	 */
	public static boolean validaMax300Carac(String s) {
		if (s.length() > 300) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui apenas caracteres alfanumericos. Retorna false se achar algum
	 * caracter que nao seja alfanumerico, caso contrario retorna true.
	 */
	public static boolean validaAlfanumericos(String s) {
		// Define o padrao da string a ser obedecido
		Pattern pattern = Pattern.compile("[^A-Za-z 0-9]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui caracteres alfanumericos e alguns especiaias. O metodo retorna
	 * false caso achar algum caracter que nao seja alfanumerico com excecao dos
	 * simbolos . , S
	 */
	public static boolean validaAlfanumericosEspeciais(String s) {
		// Define o padrao da string a ser obedecido
		Pattern pattern = Pattern.compile("[^A-Za-z 0-9$.,]");
		// Compara a string informada com o padrao definido
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui apenas numeros. Retorna false se achar algum caracter que nao seja
	 * numerico, caso contrario retorna true.
	 */
	public static boolean validaNumeros(String s) {
		// Define o padrao da string a ser obedecido
		Pattern pattern = Pattern.compile("[^0-9]");
		// Compara a string informada com o padrao definido
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui apenas numeros e obedece a formatacao de numero de telefone do
	 * tipo DDD 12341234. Retorna true se a string obedece o padrao e possui
	 * apenas numeros, caso contrario retorna false.
	 */
	public static boolean validaFormatoTelefone(String s) {
		if (s.matches("\\d{3} \\d{4}\\d{4}")) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * possui o formato de um e-mail valido(algo@algo.algo). Retorna true se a
	 * string obedece o padrao, caso contrario retorna false.
	 */
	public static boolean validaFormatoEmail(String s) {
		// Define o padrao da string a ser obedecido
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		// Compara a string informada com o padrao definido
		Matcher m = p.matcher(s);
		if (m.find()) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * equivale aos valores definidos para o campo "Tipo de Pessoa". Se a String
	 * for igual a "Fisica" retorna true e seta o valor de s na variavel
	 * tipoPessoa. Se a String for igual a "Juridica" retorna true e seta o
	 * valor de s na variavel tipoPessoa. Caso contrario retorna false.
	 */
	public static boolean validaTipoPessoa(String s) {
		String fisica = "Fisica";
		String juridica = "Juridica";
		if (s.equals(fisica)) {
			return true;
		} else if (s.equals(juridica)) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Metodo do tipo boolean para validar se a String passada como parametro
	 * obedece a formatacao de data definida do tipo dd/mm/aaaa, verifica se a
	 * data possui valores validos e transforma a String para o tipo Date.
	 * Tambem aciona o metodo que verfica se a data informada nao eh maior que a
	 * data atual. Retorna true se a data informada for valida, caso contrario
	 * retorna false.
	 */

	public static boolean validaFormatoData(String s) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			// quando o setLenient esta false o mesmo nao permite valores
			// invalidos dentro dos campos dias, mes e ano
			sdf.setLenient(false);
			Date data1 = new Date(sdf.parse(s).getTime());
			if (validaDataMaior(data1) == false) {
				throw new ClienteException("");
			}
			return true;
		} catch (ParseException e) {
			return false;
		} catch (ClienteException e) {
			return false;
		}

	}

	/*
	 * Metodo do tipo boolean para validar se a data informada eh maior que a
	 * data atual Retorna false se a data informada for invalida, caso contrario
	 * retorna true e armazena da data valida na variavel dataNascimento.
	 */
	public static boolean validaDataMaior(Date data2) {
		// pega a data atual
		Date dataAtual = new Date();
		// compara as datas
		if (data2.after(dataAtual)) {
			return false;
		} else {
			// armazena na variavel
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Tipo de Pessoa. Se o usuario deixar o campo
	 * nulo ou informar algo diferente do estabelecido sao lancadas excecoes
	 */
	public static boolean validaPessoa(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaTipoPessoa(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Nome. Se o metodo validaCampoNulo() retornar
	 * false ou o metodo validaLetras() retornar true sao lancadas excecoes,
	 * caso contrario armazena "s" na variavel nomeCompleto.
	 */
	public static boolean validaNome(String s) {

		if (validaCampoNulo(s) == false) {
			return false;
		}
		if (validaLetras(s) == true) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Sexo. Se o metodo validaCampoNulo() retornar
	 * false ou o metodo validaMasculinoFeminino() retornar false sao lancadas
	 * excecoes, caso contrario armazena "s" na variavel sexo.
	 */
	public static boolean validaSexo(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaMasculinoFeminino(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Estado Civil. Se o metodo validaLetras()
	 * retornar false eh lancada excecao, caso contrario armazena "s" na
	 * variavel estadoCivil.
	 */
	public static boolean validaEstadoCivil(String s) {

		if (validaEstadoCivilSelecionado(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo RG. Se o metodo validaCampoNulo() retornar
	 * false ou o metodo validaNumerosCaracEspeciais() retornar false sao
	 * lancadas excecoes, caso contrario armazena "s" na variavel RG.
	 */
	public static boolean validaRG(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaNumerosCaracEspeciais(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Profissao. No caso do metodo
	 * validaCampoNulo() retornar false,ou o metodo validaLetras() retornar true
	 * ou o metodo validaMax50Carac() retornar false sao lancadas excecoes. Caso
	 * contrario armazena "s" na variavel profissao.
	 */
	public static boolean validaProfissao(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaLetras(s) == true) {
			return false;
		} else if (validaMax50Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Faixa Salarial. No caso do metodo
	 * validaCampoNulo() retornar false,ou o metodo validaAlfanumericos()
	 * retornar false sao lancadas excecoes. Caso contrario armazena "s" na
	 * variavel faixaSalarial.
	 */
	public static boolean validaFaixaSalarial(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaAlfanumericos(s) == false) {
			return false;
		} else {
			return true;
		}

	}

	/*
	 * Metodo para validar o campo Time de Futebol. Se o metodo validaLetras()
	 * retornar true ou o metodo validaMax50Carac() retornar false sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel timeDeFutebol.
	 */
	public static boolean validaTimeFutebol(String s) {
		if (validaLetras(s) == true) {
			return false;
		} else if (validaMax50Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Logradouro. No caso do metodo
	 * validaCampoNulo() retornar false,ou o metodo validaAlfanumericos()
	 * retornar false ou o metodo validaMax50Carac() retornar false sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel logradouro.
	 */
	public static boolean validaLogradouro(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaAlfanumericos(s) == false) {
			return false;
		} else if (validaMax50Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Numero. No caso do metodo validaCampoNulo()
	 * retornar false,ou o metodo validaNumeros() retornar false sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel numeroEndereco.
	 */
	public static boolean validaNumeroEndereco(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaNumeros(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Complemento. Se o metodo
	 * validaAlfanumericos() retornar false,ou o metodo validaMax20Carac()
	 * retornar false sao lancadas excecoes. Caso contrario armazena "s" na
	 * variavel complemento.
	 */
	public static boolean validaComplemento(String s) {
		if (validaAlfanumericos(s) == false) {
			return false;
		} else if (validaMax20Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Bairro. Se o metodo validaAlfanumericos()
	 * retornar false,ou o metodo validaMax50Carac() retornar false sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel bairro.
	 */
	public static boolean validaBairro(String s) {
		if (validaAlfanumericos(s) == false) {
			return false;
		} else if (validaMax50Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo CEP. Se o metodo
	 * validaNumerosCaracEspeciais() retornar false eh lancada uma excecao. Caso
	 * contrario armazena "s" na variavel cEP.
	 */
	public static boolean validaCEP(String s) {
		if (validaNumerosCaracEspeciais(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Cidade. No caso do metodo validaCampoNulo()
	 * retornar false,ou o metodo validaLetras() retornar true ou o metodo
	 * validaMax50Carac() retornar false sao lancadas excecoes. Caso contrario
	 * armazena "s" na variavel cidade.
	 */
	public static boolean validaCidade(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaLetras(s) == true) {
			return false;
		} else if (validaMax50Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo UF. No caso do metodo validaCampoNulo()
	 * retornar false,ou o metodo validaLetras() retornar true sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel uF.
	 */
	public static boolean validaUF(String s) {
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaUFselecionado(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Referencias. Se o metodo
	 * validaAlfanumericos() retornar false ou o metodo validaMax50Carac()
	 * retornar false sao lancadas excecoes. Caso contrario armazena "s" na
	 * variavel referencias.
	 */
	public static boolean validaReferencias(String s) {
		if (validaAlfanumericos(s) == false) {
			return false;
		} else if (validaMax50Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Telefone Residencial. Se o metodo
	 * validaFormatoTelefone() retornar false eh lancada uma excecao. Caso
	 * contrario armazena "s" na variavel telefoneResidencial.
	 */
	public static boolean validaTelefoneResidencial(String s) {
		if (validaCampoNulo(s) == true && validaFormatoTelefone(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Telefone Celuar. Se o metodo
	 * validaFormatoTelefone() retornar false eh lancada uma excecao. Caso
	 * contrario armazena "s" na variavel telefoneCelular.
	 */
	public static boolean validaTelefoneCelular(String s) {
		if (validaCampoNulo(s) == true && validaFormatoTelefone(s) == false) {
			return false;

		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo E-mail. Se o metodo validaEmail() retornar
	 * false eh lancada uma excecao. Caso contrario armazena "s" na variavel
	 * email.
	 */
	public static boolean validaEmail(String s) {
		if (validaCampoNulo(s) == true && validaFormatoEmail(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo CPF. No caso do metodo validaCampoNulo()
	 * retornar false,ou o metodo validaNumerosCaracEspeciais() retornar false
	 * ou o metodo isCPF() da classe ValidaCPF retornar false sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel cPF.
	 */
	public static boolean validaCPF(String s) {
		String CPF;
		CPF = s;
		// Remove os caracteres da String
		CPF = CPF.replace("-", "");
		CPF = CPF.replace(".", "");
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaNumerosCaracEspeciais(s) == false) {
			return false;
		} else if (ValidaCPF.isCPF(CPF) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo CNPJ. No caso do metodo validaCampoNulo()
	 * retornar false,ou o metodo validaNumerosCaracEspeciais() retornar false
	 * ou o metodo isCNPJ() da classe ValidaCNPJ retornar false sao lancadas
	 * excecoes. Caso contrario armazena "s" na variavel cNPJ.
	 */
	public static boolean validaCNPJ(String s) {
		String CNPJ;
		CNPJ = s;
		// Remove os caracteres da String
		CNPJ = CNPJ.replace("-", "");
		CNPJ = CNPJ.replace(".", "");
		CNPJ = CNPJ.replace("/", "");
		if (validaCampoNulo(s) == false) {
			return false;
		} else if (validaNumerosCaracEspeciais(s) == false) {
			return false;
		} else if (ValidaCNPJ.isCNPJ(CNPJ) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Informacoes Adicionais. No caso do metodo
	 * validaAlfanumericos() retornar false ou o metodo validaMax300Carac()
	 * retornar false sao lancadas excecoes. Caso contrario armazena "s" na
	 * variavel infoAdicionais.
	 */
	public static boolean validaInfoAdicionais(String s) {
		if (validaAlfanumericos(s) == false) {
			return false;
		} else if (validaMax300Carac(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Data de Nascimento. Se o metodo
	 * validaCampoNulo() retorna true pelo fato de nao se tratar de um campo
	 * obrigatorio entao o metodo validaDataNascimento() sera executado e caso o
	 * mesmo retorne false eh lancada uma excecao.
	 */
	public static boolean validaDataNascimento(String s) {
		if (validaCampoNulo(s) == true && validaFormatoData(s) == false) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Metodo para validar o campo Montante. Se o metodo
	 * validaAlfanumericosEspeciais() retornar false eh lancada uma nova
	 * excecao, se nao armazena "s" na variavel montante.
	 */
	public static boolean validaMontante(String s) {
		if (validaAlfanumericosEspeciais(s) == false) {
			return false;
		} else {
			return true;
		}
	}

}
