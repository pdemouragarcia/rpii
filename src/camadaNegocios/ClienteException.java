package camadaNegocios;

import javax.swing.JOptionPane;

/*
 * @author Bernard 
 * @author Luis
 * @author Pedro
 * @author Rafael
 * @author Rodrigo
 */

public class ClienteException extends Exception {

	/**
	 * excecao que dispara mensagens de erro contidas dentro das validacoes no
	 * console.
	 */
	private static final long serialVersionUID = 1L;

	ClienteException(String message) {
		super(message);
		JOptionPane.showMessageDialog(null, message, "", JOptionPane.ERROR_MESSAGE);
	}
}
