package camadaNegocios;
/*
 * @author Bernard 
 * @author Luis
 * @author Pedro
 * @author Rafael
 * @author Rodrigo
 */

import java.util.Date;

import camadaPersistencia.ArrayClientes;

public class Cliente {

	private String nomeCompleto;
	private String sexo;
	private String estadoCivil;
	private String dataNascimento;
	private String RG;
	private String profissao;
	private String faixaSalarial;
	private String timeDeFutebol;
	private String logradouro;
	private String numeroEndereco;
	private String complemento;
	private String bairro;
	private String cEP;
	private String cidade;
	private String uF;
	private String referencias;
	private String telefoneResidencial;
	private String telefoneCelular;
	private String email;
	private String tipoPessoa;
	public static int id = 1;
	public int idCliente;
	public Date dataCadastro;
	private String montante;
	private String infoAdicionais;
	private String cPF;
	private String cNPJ;

	public void addP(String nome, String sexo, String estadoCivil, String dataNascimento, String RG, String profissao,
			String faixaSalarial, String timeDeFutebol, String logradouro, String numeroEndereco, String complemento,
			String bairro, String cEP, String cidade, String uF, String referencias, String telefoneResidencial,
			String telefoneCelular, String email, String tipoPessoa, String montante, String infoAdicionais, String cPF,
			String cNPJ) {
		if (validaCliente(nome, sexo, estadoCivil, dataNascimento, RG, profissao, faixaSalarial, timeDeFutebol,
				logradouro, numeroEndereco, complemento, bairro, cEP, cidade, uF, referencias, telefoneResidencial,
				telefoneCelular, email, tipoPessoa, montante, infoAdicionais, cPF, cNPJ) == true) {
			this.dataCadastro = new Date();
			this.idCliente = Cliente.id++;
			ArrayClientes.addCliente(this);
		}

	}

	public void updateaP(int id, String nome, String sexo, String estadoCivil, String dataNascimento, String RG,
			String profissao, String faixaSalarial, String timeDeFutebol, String logradouro, String numeroEndereco,
			String complemento, String bairro, String cEP, String cidade, String uF, String referencias,
			String telefoneResidencial, String telefoneCelular, String email, String tipoPessoa, String montante,
			String infoAdicionais, String cPF, String cNPJ) {
		if (validaCliente(nome, sexo, estadoCivil, dataNascimento, RG, profissao, faixaSalarial, timeDeFutebol,
				logradouro, numeroEndereco, complemento, bairro, cEP, cidade, uF, referencias, telefoneResidencial,
				telefoneCelular, email, tipoPessoa, montante, infoAdicionais, cPF, cNPJ) == true) {
			this.dataCadastro = new Date();
			this.idCliente = id;
			ArrayClientes.atualizaCliente(id, this);
		}

	}

	private boolean validaCliente(String nome, String sexo, String estadoCivil, String dataNascimento, String RG,
			String profissao, String faixaSalarial, String timeDeFutebol, String logradouro, String numeroEndereco,
			String complemento, String bairro, String cEP, String cidade, String uF, String referencias,
			String telefoneResidencial, String telefoneCelular, String email, String tipoPessoa, String montante,
			String infoAdicionais, String cPF, String cNPJ) {
		try {
			setNomeCompleto(nome);
			setSexo(sexo);
			setEstadoCivil(estadoCivil);
			setDataNascimento(dataNascimento);
			setEstadoCivil(estadoCivil);
			setRG(RG);
			setProfissao(profissao);
			setFaixaSalarial(faixaSalarial);
			setTimeDeFutebol(timeDeFutebol);
			setLogradouro(logradouro);
			setNumeroEndereco(numeroEndereco);
			setComplemento(complemento);
			setBairro(bairro);
			setcEP(cEP);
			setCidade(cidade);
			setuF(uF);
			setReferencias(referencias);
			setTelefoneResidencial(telefoneResidencial);
			setTelefoneCelular(telefoneCelular);
			setEmail(email);
			setTipoPessoa(tipoPessoa);
			setMontante(montante);
			setInfoAdicionais(infoAdicionais);
			if (tipoPessoa == "Fisica") {
				setcPF(cPF);
			} else if (tipoPessoa == "Juridica") {
				setcNPJ(cNPJ);
			}
			return true;

		} catch (ClienteException e) {
			return false;
		}
	}

	public String getcNPJ() {
		return cNPJ;
	}

	public void setcNPJ(String cNPJ) throws ClienteException {
		if (ValidaClientes.validaCNPJ(cNPJ) == true) {
			this.cNPJ = cNPJ;
		} else {
			throw new ClienteException(" CNPJ*:\n  Campo Obrigatorio \n ");
		}
	}

	public String getcPF() {
		return cPF;
	}

	public void setcPF(String cPF) throws ClienteException {
		if (ValidaClientes.validaCPF(cPF) == true) {
			this.cPF = cPF;
		} else {
			throw new ClienteException(" CPF*:\n  Campo Obrigatorio \n CPF Invalido ");
		}
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) throws ClienteException {
		if (ValidaClientes.validaNome(nomeCompleto) == true) {
			this.nomeCompleto = nomeCompleto;
		} else {
			throw new ClienteException("Nome*: \n Campo Obrigatorio \n Apenas Letras");
		}
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) throws ClienteException {
		if (ValidaClientes.validaSexo(sexo) == true) {
			this.sexo = sexo;
		} else {
			throw new ClienteException("Sexo*: \n  Campo Obrigatorio \n ");
		}
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) throws ClienteException {
		if (ValidaClientes.validaEstadoCivil(estadoCivil) == true) {
			this.estadoCivil = estadoCivil;
		} else {
			throw new ClienteException("Estado Civil: \n  \n ");
		}
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) throws ClienteException {
		if (ValidaClientes.validaDataNascimento(dataNascimento) == true) {
			this.dataNascimento = dataNascimento;
		} else {
			throw new ClienteException("Data de Nascimento: \n  \n ");
		}
	}

	public String getRG() {
		return RG;
	}

	public void setRG(String rG) throws ClienteException {
		if (ValidaClientes.validaRG(rG) == true) {
			RG = rG;
		} else {
			throw new ClienteException("RG*: \n  Campo Obrigatorio \n ");
		}
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) throws ClienteException {
		if (ValidaClientes.validaProfissao(profissao) == true) {
			this.profissao = profissao;
		} else {
			throw new ClienteException("Profissao*: \n Campo Obrigatorio  \n ");
		}
	}

	public String getFaixaSalarial() {
		return faixaSalarial;
	}

	public void setFaixaSalarial(String faixaSalarial) throws ClienteException {
		if (ValidaClientes.validaFaixaSalarial(faixaSalarial) == true) {
			this.faixaSalarial = faixaSalarial;
		} else {
			throw new ClienteException("Faixa Salarial*: \n Campo Obrigatorio  \n ");
		}
	}

	public String getTimeDeFutebol() {
		return timeDeFutebol;
	}

	public void setTimeDeFutebol(String timeDeFutebol) throws ClienteException {
		if (ValidaClientes.validaTimeFutebol(timeDeFutebol) == true) {
			this.timeDeFutebol = timeDeFutebol;
		} else {
			throw new ClienteException("Time de Futebol: \n  \n ");
		}
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) throws ClienteException {
		if (ValidaClientes.validaLogradouro(logradouro) == true) {
			this.logradouro = logradouro;
		} else {
			throw new ClienteException("Logradouro*: \n  Campo Obrigatorio \n ");
		}
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String nmro) throws ClienteException {
		if (ValidaClientes.validaNumeroEndereco(nmro) == true) {
			this.numeroEndereco = nmro;
		} else {
			throw new ClienteException("Numero*: \n  Campo Obrigatorio \n ");
		}
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) throws ClienteException {
		if (ValidaClientes.validaComplemento(complemento) == true) {
			this.complemento = complemento;
		} else {
			throw new ClienteException("Complemento: \n  \n ");
		}
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) throws ClienteException {
		if (ValidaClientes.validaBairro(bairro) == true) {
			this.bairro = bairro;
		} else {
			throw new ClienteException("Bairro: \n  \n ");
		}
	}

	public String getcEP() {
		return cEP;
	}

	public void setcEP(String cEP) throws ClienteException {
		if (ValidaClientes.validaCEP(cEP) == true) {
			this.cEP = cEP;
		} else {
			throw new ClienteException("CEP: \n  \n ");
		}
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) throws ClienteException {
		if (ValidaClientes.validaCidade(cidade) == true) {
			this.cidade = cidade;
		} else {
			throw new ClienteException("Cidade*:\n  Campo Obrigatorio \n ");
		}
	}

	public String getuF() {
		return uF;
	}

	public void setuF(String uF) throws ClienteException {
		if (ValidaClientes.validaUF(uF) == true) {
			this.uF = uF;
		} else {
			throw new ClienteException("UF*: \n  Campo Obrigatorio \n ");
		}
	}

	public String getReferencias() {
		return referencias;
	}

	public void setReferencias(String referencias) throws ClienteException {
		if (ValidaClientes.validaReferencias(referencias) == true) {
			this.referencias = referencias;
		} else {
			throw new ClienteException("Referencias*: \n  \n ");
		}
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) throws ClienteException {
		if (ValidaClientes.validaTelefoneResidencial(telefoneResidencial) == true) {
			this.telefoneResidencial = telefoneResidencial;
		} else {
			throw new ClienteException("Telefone Residencial: \n  \n ");
		}
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) throws ClienteException {
		if (ValidaClientes.validaTelefoneCelular(telefoneCelular) == true) {
			this.telefoneCelular = telefoneCelular;
		} else {
			throw new ClienteException("Telefone Celular: \n  \n ");
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws ClienteException {
		if (ValidaClientes.validaEmail(email) == true) {
			this.email = email;
		} else {
			throw new ClienteException("E-mail: \n  \n ");
		}
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) throws ClienteException {
		if (ValidaClientes.validaTipoPessoa(tipoPessoa) == true) {
			this.tipoPessoa = tipoPessoa;
		} else {
			throw new ClienteException("Tipo de Pessoa*: \n  Campo Obrigatorio \n ");
		}
	}

	public int getId() {
		return idCliente;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public String getMontante() {
		return montante;
	}

	public void setMontante(String montante) throws ClienteException {
		if (ValidaClientes.validaMontante(montante)) {
			this.montante = montante;
		} else {
			throw new ClienteException("Montante: \n  \n ");
		}
	}

	public String getInfoAdicionais() {
		return infoAdicionais;
	}

	public void setInfoAdicionais(String infoAdicionais) throws ClienteException {
		if (ValidaClientes.validaInfoAdicionais(infoAdicionais)) {
			this.infoAdicionais = infoAdicionais;
		} else {
			throw new ClienteException("Informacoes Adicionais \n  \n ");
		}
	}

}