package camadaApresentacao;

import java.awt.*;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import camadaNegocios.Cliente;
import camadaPersistencia.ArrayClientes;

@SuppressWarnings("serial")
public class JanelaRelatorioGeral extends JFrame implements ActionListener {
	JPanel painelFundo;
	JTable tabela;
	JScrollPane barraRolagem;
	private JButton buttonVoltar;

	public JanelaRelatorioGeral() {
		super("Relatorio Geral");
		Container content = getContentPane();
		content.setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(1, 1));
		painelFundo.setBounds(50, 50, 1150, 550);

		// Define as colunas da tabela
		String[] col = { "ID", "Nome", "Tipo Pessoa", "CPF | CNPJ", "Data Cadastro", "Sexo", "Estado Civil",
				"Data Nascimento", "RG", "Profissao", "Faixa Salarial", "Logradouro", "Numero", "Complemento", "Bairro",
				"Cidade", "UF", "Referencias", "Telefone Residencial", "Telefone Celular", "E-mail", "Time Futebol",
				"Montante", "InfoAdicio" };
		DefaultTableModel tableModel = new DefaultTableModel(col, 0) {
			// Define que as celulas da tabela nao sao editaveis
			public boolean isCellEditable(int rowIndex, int mColIndex) {
				return false;
			}
		};
		JTable table = new JTable(tableModel);

		ArrayList<Cliente> clientes = (ArrayList<Cliente>) ArrayClientes.getClientes();
		// Percorre o array clientes
		for (int i = 0; i < clientes.size(); i++) {
			// Armazena os valores do objeto cliente nas variaveis
			String Pessoa = clientes.get(i).getTipoPessoa();
			String CNPJCPF = null;
			if (Pessoa == "Fisica") {
				CNPJCPF = clientes.get(i).getcPF();
			} else if (Pessoa == "Juridica") {
				CNPJCPF = clientes.get(i).getcNPJ();
			}
			int ID = clientes.get(i).getId();
			String Nome = clientes.get(i).getNomeCompleto();
			String Sexo = clientes.get(i).getSexo();
			String EstadoCivil = clientes.get(i).getEstadoCivil();
			String Data = clientes.get(i).getDataNascimento();
			String RG = clientes.get(i).getRG();
			String Profissao = clientes.get(i).getProfissao();
			String Faixa = clientes.get(i).getFaixaSalarial();
			String Logradouro = clientes.get(i).getLogradouro();
			String Numero = clientes.get(i).getNumeroEndereco();
			String Complemento = clientes.get(i).getComplemento();
			String Bairro = clientes.get(i).getBairro();
			String Cidade = clientes.get(i).getCidade();
			String UF = clientes.get(i).getuF();
			String Referencias = clientes.get(i).getReferencias();
			String TelefoneR = clientes.get(i).getTelefoneResidencial();
			String TelefoneC = clientes.get(i).getTelefoneCelular();
			String Email = clientes.get(i).getEmail();
			String Time = clientes.get(i).getTimeDeFutebol();
			String Montante = clientes.get(i).getMontante();
			String InfoAdicio = clientes.get(i).getInfoAdicionais();
			Date DataCadastro = clientes.get(i).getDataCadastro();
			// Define as linhas da tabela com as informacoes do array
			Object[] data = { ID, Nome, Pessoa, CNPJCPF, DataCadastro, Sexo, EstadoCivil, Data, RG, Profissao, Faixa,
					Logradouro, Numero, Complemento, Bairro, Cidade, UF, Referencias, TelefoneR, TelefoneC, Email, Time,
					Montante, InfoAdicio };
			// Insere cada cliente em uma nova linha da tabela
			tableModel.addRow(data);
		}

		barraRolagem = new JScrollPane(table);
		painelFundo.add(barraRolagem);
		getContentPane().add(painelFundo);
		// Botao voltar
		buttonVoltar = new JButton("Voltar");
		buttonVoltar.setBounds(new Rectangle(500, 600, 200, 40));
		content.add(buttonVoltar, null);
		buttonVoltar.setActionCommand("EXIT");
		buttonVoltar.addActionListener(this);

		// Define o tamanho da tela
		setSize(1300, 700);
		setVisible(true);
		// Desabilita a maximizacao
		setResizable(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();

		if (comando.equals("EXIT")) {
			// Retorna ao menu principal e fecha a janela atual
			new JanelaMenuGeral();
			this.dispose();
		}
	}
}
