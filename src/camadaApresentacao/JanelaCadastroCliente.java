package camadaApresentacao;

import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import camadaNegocios.Cliente;

@SuppressWarnings("serial")
public class JanelaCadastroCliente extends JFrame implements ActionListener {
	private JLabel label, labelCPF, labelCNPJ;
	private JTextField fieldNome, fieldDataNascimento, fieldRG, fieldProfissao, fieldFaixaSalarial, fieldTimeFutebol,
			fieldLogradouro, fieldNumero, fieldComplemento, fieldBairro, fieldCEP, fieldCidade, fieldReferencias,
			fieldTelefoneR, fieldTelefoneC, fieldEmail, fieldCPF, fieldCNPJ, fieldMontante, fieldInfoAdicionais;
	private JButton buttonCadastrar, buttonCancelar;
	@SuppressWarnings("rawtypes")
	private JComboBox jComboBoxSexo, jComboBoxEstadoCivil, jComboBoxTipoPessoa, jComboBoxUF;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public JanelaCadastroCliente() {
		super("Cadastrar Novo Cliente");
		Container content = getContentPane();

		jComboBoxSexo = new JComboBox();
		jComboBoxEstadoCivil = new JComboBox();
		jComboBoxTipoPessoa = new JComboBox();
		jComboBoxUF = new JComboBox();

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// this.setExtendedState(MAXIMIZED_BOTH);

		content.setLayout(null);
		setSize(1100, 700);

		Font font = new Font("Serif", Font.PLAIN, 10);
		content.setFont(font);

		String labelText20 = "Tipo de Pessoa*:";
		label = new JLabel(labelText20);
		label.setBounds(new Rectangle(1, 40, 125, 25));
		content.add(label);
		jComboBoxTipoPessoa.setModel(new DefaultComboBoxModel(new String[] { "", "Fisica", "Juridica" }));
		jComboBoxTipoPessoa.setBounds(150, 40, 200, 25);
		content.add(jComboBoxTipoPessoa);
		jComboBoxTipoPessoa.addActionListener(this);

		String labelText21 = "CPF*:";
		labelCPF = new JLabel(labelText21);
		labelCPF.setBounds(new Rectangle(1, 80, 125, 25));
		content.add(labelCPF);
		labelCPF.setVisible(false);
		fieldCPF = new JTextField();
		fieldCPF.setBounds(new Rectangle(150, 80, 200, 25));
		fieldCPF.setVisible(false);
		content.add(fieldCPF, null);

		String labelText22 = "CNPJ*:";
		labelCNPJ = new JLabel(labelText22);
		labelCNPJ.setBounds(new Rectangle(1, 80, 125, 25));
		content.add(labelCNPJ);
		labelCNPJ.setVisible(false);
		fieldCNPJ = new JTextField();
		fieldCNPJ.setBounds(new Rectangle(150, 80, 200, 25));
		fieldCNPJ.setVisible(false);
		content.add(fieldCNPJ, null);

		String labelText = "Nome Completo*: ";
		label = new JLabel(labelText);
		label.setBounds(new Rectangle(1, 110, 125, 25));
		content.add(label);
		fieldNome = new JTextField();
		fieldNome.setBounds(new Rectangle(150, 110, 200, 25));
		content.add(fieldNome, null);

		String labelText2 = "Sexo*: ";
		label = new JLabel(labelText2);
		label.setBounds(new Rectangle(500, 110, 125, 25));
		content.add(label);
		jComboBoxSexo.setModel(new DefaultComboBoxModel(new String[] { "", "Masculino", "Feminino" }));
		jComboBoxSexo.setBounds(650, 110, 200, 25);
		content.add(jComboBoxSexo);

		String labelText3 = "Estado Civil:";
		label = new JLabel(labelText3);
		label.setBounds(new Rectangle(1, 140, 125, 25));
		content.add(label);
		jComboBoxEstadoCivil.setModel(new DefaultComboBoxModel(new String[] { "", "Solteiro(a)", "Casado(a)",
				"Divorciado(a)", "Viuvo(a)", "Separado(a)", "Companheiro(a)" }));
		jComboBoxEstadoCivil.setBounds(150, 140, 200, 25);
		content.add(jComboBoxEstadoCivil);

		String labelText4 = "Data de Nascimento:";
		label = new JLabel(labelText4);
		label.setBounds(new Rectangle(500, 140, 125, 25));
		content.add(label);
		fieldDataNascimento = new JTextField();
		fieldDataNascimento.setBounds(new Rectangle(650, 140, 200, 25));
		content.add(fieldDataNascimento, null);

		String labelText5 = "RG*:";
		label = new JLabel(labelText5);
		label.setBounds(new Rectangle(1, 170, 125, 25));
		content.add(label);
		fieldRG = new JTextField();
		fieldRG.setBounds(new Rectangle(150, 170, 200, 25));
		content.add(fieldRG, null);

		String labelText6 = "Profissao*:";
		label = new JLabel(labelText6);
		label.setBounds(new Rectangle(500, 170, 125, 25));
		content.add(label);
		fieldProfissao = new JTextField();
		fieldProfissao.setBounds(new Rectangle(650, 170, 200, 25));
		content.add(fieldProfissao, null);

		String labelText7 = "Faixa Salarial*:";
		label = new JLabel(labelText7);
		label.setBounds(new Rectangle(1, 200, 125, 25));
		content.add(label);
		fieldFaixaSalarial = new JTextField();
		fieldFaixaSalarial.setBounds(new Rectangle(150, 200, 200, 25));
		content.add(fieldFaixaSalarial, null);

		String labelText8 = "Time de Futebol:";
		label = new JLabel(labelText8);
		label.setBounds(new Rectangle(500, 200, 125, 25));
		content.add(label);
		fieldTimeFutebol = new JTextField();
		fieldTimeFutebol.setBounds(new Rectangle(650, 200, 200, 25));
		content.add(fieldTimeFutebol, null);

		String labelText9 = "Logradouro*:";
		label = new JLabel(labelText9);
		label.setBounds(new Rectangle(1, 230, 125, 25));
		content.add(label);
		fieldLogradouro = new JTextField();
		fieldLogradouro.setBounds(new Rectangle(150, 230, 200, 25));
		content.add(fieldLogradouro, null);

		String labelText10 = "Numero*:";
		label = new JLabel(labelText10);
		label.setBounds(new Rectangle(500, 230, 125, 25));
		content.add(label);
		fieldNumero = new JTextField();
		fieldNumero.setBounds(new Rectangle(650, 230, 200, 25));
		content.add(fieldNumero, null);

		String labelText11 = "Complemento:";
		label = new JLabel(labelText11);
		label.setBounds(new Rectangle(1, 260, 125, 25));
		content.add(label);
		fieldComplemento = new JTextField();
		fieldComplemento.setBounds(new Rectangle(150, 260, 200, 25));
		content.add(fieldComplemento, null);

		String labelText12 = "Bairro:";
		label = new JLabel(labelText12);
		label.setBounds(new Rectangle(500, 260, 125, 25));
		content.add(label);
		fieldBairro = new JTextField();
		fieldBairro.setBounds(new Rectangle(650, 260, 200, 25));
		content.add(fieldBairro, null);

		String labelText13 = "CEP:";
		label = new JLabel(labelText13);
		label.setBounds(new Rectangle(1, 290, 125, 25));
		content.add(label);
		fieldCEP = new JTextField();
		fieldCEP.setBounds(new Rectangle(150, 290, 200, 25));
		content.add(fieldCEP, null);

		String labelText14 = "Cidade*:";
		label = new JLabel(labelText14);
		label.setBounds(new Rectangle(500, 290, 125, 25));
		content.add(label);
		fieldCidade = new JTextField();
		fieldCidade.setBounds(new Rectangle(650, 290, 200, 25));
		content.add(fieldCidade, null);

		String labelText15 = "UF*:";
		label = new JLabel(labelText15);
		label.setBounds(new Rectangle(1, 320, 125, 25));
		content.add(label);
		jComboBoxUF.setModel(new DefaultComboBoxModel(
				new String[] { "", "Acre", "Alagoas", "Amazonas", "Amap�", "Bahia", "Cear�", "Distrito Federal",
						"Esp�rito Santo", "Goi�s", "Maranh�o", "Minas Gerais", "Mato Grosso do Sul", "Mato Grosso",
						"Para�ba", "Pernambuco", "Piau�", "Paran�", "Rio de Janeiro", "Rio Grande do Norte", "Rond�nia",
						"Roraima", "Rio Grande do Sul", "Santa Catarina", "Sergipe", "S�o Paulo", "Tocantins" }));
		jComboBoxUF.setBounds(150, 320, 200, 25);
		content.add(jComboBoxUF);

		String labelText16 = "Referencias:";
		label = new JLabel(labelText16);
		label.setBounds(new Rectangle(500, 320, 125, 25));
		content.add(label);
		fieldReferencias = new JTextField();
		fieldReferencias.setBounds(new Rectangle(650, 320, 200, 25));
		content.add(fieldReferencias, null);

		String labelText17 = "Telefone Residencial:";
		label = new JLabel(labelText17);
		label.setBounds(new Rectangle(1, 350, 125, 25));
		content.add(label);
		fieldTelefoneR = new JTextField();
		fieldTelefoneR.setBounds(new Rectangle(150, 350, 200, 25));
		content.add(fieldTelefoneR, null);

		String labelText18 = "Telefone Celular:";
		label = new JLabel(labelText18);
		label.setBounds(new Rectangle(500, 350, 125, 25));
		content.add(label);
		fieldTelefoneC = new JTextField();
		fieldTelefoneC.setBounds(new Rectangle(650, 350, 200, 25));
		content.add(fieldTelefoneC, null);

		String labelText19 = "E-mail:";
		label = new JLabel(labelText19);
		label.setBounds(new Rectangle(1, 380, 125, 25));
		content.add(label);
		fieldEmail = new JTextField();
		fieldEmail.setBounds(new Rectangle(150, 380, 200, 25));
		content.add(fieldEmail, null);

		String labelText23 = "Montante:";
		label = new JLabel(labelText23);
		label.setBounds(new Rectangle(1, 410, 125, 25));
		content.add(label);
		fieldMontante = new JTextField();
		fieldMontante.setBounds(new Rectangle(150, 410, 200, 25));
		content.add(fieldMontante, null);

		String labelText24 = "Info. Adicionais:";
		label = new JLabel(labelText24);
		label.setBounds(new Rectangle(500, 380, 125, 25));
		content.add(label);
		fieldInfoAdicionais = new JTextField();
		fieldInfoAdicionais.setBounds(new Rectangle(650, 380, 200, 25));
		content.add(fieldInfoAdicionais, null);

		buttonCadastrar = new JButton("Cadastrar");
		buttonCadastrar.setBounds(new Rectangle(200, 500, 100, 25));
		content.add(buttonCadastrar, null);
		buttonCadastrar.setActionCommand("OK");
		buttonCadastrar.addActionListener(this);
		// buttonOk.setMnemonic(KeyEvent.VK_F);

		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setBounds(new Rectangle(510, 500, 100, 25));
		content.add(buttonCancelar, null);
		buttonCancelar.setActionCommand("EXIT");
		buttonCancelar.addActionListener(this);

		setVisible(true);
		setResizable(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		fieldCPF.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 1);
		labelCPF.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 1);
		fieldCNPJ.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 2);
		labelCNPJ.setVisible(jComboBoxTipoPessoa.getSelectedIndex() == 2);

		if (comando.equals("EXIT")) {
			new JanelaMenuGeral();
			this.dispose();
			// new JanelaMenuGeral();
		}
		if (comando.equals("OK")) {
			String nome = fieldNome.getText();
			String sexo = jComboBoxSexo.getSelectedItem().toString();
			String estadoCivil = jComboBoxEstadoCivil.getSelectedItem().toString();
			String dataNas = fieldDataNascimento.getText();
			String rg = fieldRG.getText();
			String profissao = fieldProfissao.getText();
			String faixa = fieldFaixaSalarial.getText();
			String time = fieldTimeFutebol.getText();
			String logra = fieldLogradouro.getText();
			String nmro = fieldNumero.getText();
			String complemento = fieldComplemento.getText();
			String bairro = fieldBairro.getText();
			String cep = fieldCEP.getText();
			String cidade = fieldCidade.getText();
			String uf = jComboBoxUF.getSelectedItem().toString();
			String refe = fieldReferencias.getText();
			String tr = fieldTelefoneR.getText();
			String tc = fieldTelefoneC.getText();
			String email = fieldEmail.getText();
			String tipoP0 = jComboBoxTipoPessoa.getSelectedItem().toString();
			String cpf = fieldCPF.getText();
			String cnpj = fieldCNPJ.getText();
			String montan = fieldMontante.getText();
			String infoAd = fieldInfoAdicionais.getText();

			Cliente p = new Cliente();
			p.addP(nome, sexo, estadoCivil, dataNas, rg, profissao, faixa, time, logra, nmro, complemento, bairro, cep,
					cidade, uf, refe, tr, tc, email, tipoP0, montan, infoAd, cpf, cnpj);

		}
	}
}
