package camadaApresentacao;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import camadaNegocios.Cliente;
import camadaPersistencia.ArrayClientes;

@SuppressWarnings("serial")
public class JanelaConsulta extends JFrame implements ActionListener {
	JTextField fieldBuscaID;
	JPanel painelFundo;
	JTable table;
	JScrollPane barraRolagem;
	private JButton buttonSelecionar, buttonVoltar, buttonRemover, buttonEditar;
	public int idpassada;
	boolean variavel;
	DefaultTableModel tableModel;

	public JanelaConsulta() {
		super("Pesquisa Id");
		Container content = getContentPane();
		content.setLayout(null);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(1, 1));
		painelFundo.setBounds(50, 50, 1150, 550);

		// Define as colunas da tabela
		String[] col = { "ID", "Nome", "Tipo Pessoa", "CPF | CNPJ", "Data Cadastro", "Sexo", "RG", "Profissao",
				"Faixa Salarial", "Logradouro", "Numero", "Cidade", "UF" };
		tableModel = new DefaultTableModel(col, 0) {
			// Define que as celulas da tabela nao sao editaveis
			public boolean isCellEditable(int rowIndex, int mColIndex) {
				return false;
			}
		};
		table = new JTable(tableModel);

		// Percorre o array de clientes
		ArrayList<Cliente> clientes = (ArrayList<Cliente>) ArrayClientes.getClientes();
		for (int i = 0; i < clientes.size(); i++) {

			// armazena as informacoes do array nas strings
			String Pessoa = clientes.get(i).getTipoPessoa();
			String CNPJCPF = null;
			if (Pessoa == "Fisica") {
				CNPJCPF = clientes.get(i).getcPF();
			} else if (Pessoa == "Juridica") {
				CNPJCPF = clientes.get(i).getcNPJ();
			}
			int ID = clientes.get(i).getId();
			String Nome = clientes.get(i).getNomeCompleto();
			String Sexo = clientes.get(i).getSexo();
			String RG = clientes.get(i).getRG();
			String Profissao = clientes.get(i).getProfissao();
			String Faixa = clientes.get(i).getFaixaSalarial();
			String Logradouro = clientes.get(i).getLogradouro();
			String Numero = clientes.get(i).getNumeroEndereco();
			String Cidade = clientes.get(i).getCidade();
			String UF = clientes.get(i).getuF();
			Date DataCadastro = clientes.get(i).getDataCadastro();
			// Define as linhas da tabela com as informacoes do array
			Object[] data = { ID, Nome, Pessoa, CNPJCPF, DataCadastro, Sexo, RG, Profissao, Faixa, Logradouro, Numero,
					Cidade, UF };
			// Insere cada cliente em uma nova linha da tabela
			tableModel.addRow(data);
		}

		barraRolagem = new JScrollPane(table);
		painelFundo.add(barraRolagem);
		getContentPane().add(painelFundo);

		// Botao buscar por id
		buttonSelecionar = new JButton("Buscar ID");
		buttonSelecionar.setBounds(new Rectangle(100, 610, 200, 40));
		content.add(buttonSelecionar, null);
		buttonSelecionar.setActionCommand("BUSCAR");
		buttonSelecionar.addActionListener(this);
		// Botao editar selecionado
		buttonEditar = new JButton("Editar Selecionado");
		buttonEditar.setBounds(new Rectangle(700, 610, 200, 40));
		content.add(buttonEditar, null);
		buttonEditar.setActionCommand("EDITAR");
		buttonEditar.addActionListener(this);
		// Botao remover selecionado
		buttonRemover = new JButton("Remover Selecionado");
		buttonRemover.setBounds(new Rectangle(400, 610, 200, 40));
		content.add(buttonRemover, null);
		buttonRemover.setActionCommand("REMOVER");
		buttonRemover.addActionListener(this);
		// Botao voltar
		buttonVoltar = new JButton("Voltar");
		buttonVoltar.setBounds(new Rectangle(1000, 610, 200, 40));
		content.add(buttonVoltar, null);
		buttonVoltar.setActionCommand("EXIT");
		buttonVoltar.addActionListener(this);
		// Campo para informar o id desejado
		fieldBuscaID = new JTextField();
		fieldBuscaID.setBounds(new Rectangle(100, 680, 200, 40));
		content.add(fieldBuscaID, null);
		// Tamanho da janela
		setSize(1300, 800);
		setVisible(true);
		// Desabilita a maximizacao
		setResizable(false);
	}

	public void verificaRemoveLinha() {
		int x;
		// Pega a linha selecionada na tabela
		x = table.getSelectedRow();
		// Pega o id do cliente da linha selecionada na tabela
		int y = (int) table.getValueAt(x, 0);
		// Pega o nome do cliente da linha selecionada na tabela
		String cli = (String) table.getValueAt(x, 1);
		// Mensagem de confirmacao
		String message = "Deseja remover permanentemente o Cliente: \n" + cli + "\nID: " + y;
		// Titulo
		String title = "Excluir Cliente";
		// Aviso de confirmacao de exclusao com o title e a message definidos
		// anteriormente
		int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
		// se a resposta for = a sim
		if (reply == JOptionPane.YES_OPTION) {
			// se o metodo de remocao voltar true para o parametro y passado
			if (removeDoArray(y) == true) {
				// retorna msg de aviso
				JOptionPane.showMessageDialog(null, " Cadastro Removido ", "", JOptionPane.INFORMATION_MESSAGE);
				// cria uma nova janela e desabilita a atual para atualizar a
				// tabela
				new JanelaConsulta();
				this.dispose();
			}
		}
	}

	public boolean removeDoArray(int a) {
		ArrayList<Cliente> clientes = (ArrayList<Cliente>) ArrayClientes.getClientes();
		// percorre o array de clientes
		for (int c = 0; c <= clientes.size(); c++) {
			// se o parametro a for igual ao id de algum clinte do array
			if (clientes.get(c).getId() == a) {
				// remove o cliente do array e retorna true
				clientes.remove(c);
				return true;
			}
		}
		return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();

		if (comando.equals("EXIT")) {
			// Fecha a janela atual e retorna para o menu
			new JanelaMenuGeral();
			this.dispose();
		}
		if (comando.equals("REMOVER")) {
			// Se o numero de linhas selecionadas for = a 0
			if (table.getSelectedRows().length == 0) {
				// Retorna msg
				JOptionPane.showMessageDialog(null, " Selecione um Cliente ", "", JOptionPane.INFORMATION_MESSAGE);
				// Se o numero de linhas selecionadas for = a 1
			} else if (table.getSelectedRowCount() == 1) {
				// aciona o metodo
				verificaRemoveLinha();
				// Caso contrario
			} else {
				// retorna msg
				JOptionPane.showMessageDialog(null, " Selecione Apenas Um Cliente ", "",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
		if (comando.equals("BUSCAR")) {
			// Se o campo esta em branco
			if (fieldBuscaID.getText().isEmpty()) {
				// retorna msg
				JOptionPane.showMessageDialog(null, " Informe um ID ", "", JOptionPane.ERROR_MESSAGE);
				// Caso contrario
			} else {
				ArrayList<Cliente> clientes = (ArrayList<Cliente>) ArrayClientes.getClientes();
				// percorre o array
				for (int i = 0; i < clientes.size(); i++) {
					// armazena o valor informado no campo ID na variavel id2
					String id2 = fieldBuscaID.getText();
					// converte a String id2 para o tipo int e armazena em
					// idUser
					int idUser = Integer.parseInt(id2);
					// Se achar um idUser = ao Id de um cliente
					if (idUser == clientes.get(i).getId()) {
						// armazena nas variaveis as informacoes desse cliente
						String Pessoa = clientes.get(i).getTipoPessoa();
						String CNPJCPF = null;
						if (Pessoa == "Fisica") {
							CNPJCPF = clientes.get(i).getcPF();
						} else if (Pessoa == "Juridica") {
							CNPJCPF = clientes.get(i).getcNPJ();
						}
						int ID = clientes.get(i).getId();
						String Nome = clientes.get(i).getNomeCompleto();
						String Sexo = clientes.get(i).getSexo();
						String RG = clientes.get(i).getRG();
						String Profissao = clientes.get(i).getProfissao();
						String Faixa = clientes.get(i).getFaixaSalarial();
						String Logradouro = clientes.get(i).getLogradouro();
						String Numero = clientes.get(i).getNumeroEndereco();
						String Cidade = clientes.get(i).getCidade();
						String UF = clientes.get(i).getuF();
						Date DataCadastro = clientes.get(i).getDataCadastro();

						// Adiciona as variaveis no vetor data
						Object[] data = { ID, Nome, Pessoa, CNPJCPF, DataCadastro, Sexo, RG, Profissao, Faixa,
								Logradouro, Numero, Cidade, UF };
						// Limpa as linhas da tabela
						tableModel.setNumRows(0);
						// Adiciona o vetor data como linha da tabela
						tableModel.addRow(data);
					}
				}
			}
		}
		if (comando.equals("EDITAR")) {
			// Se o numero de linhas selecionadas for = a 0
			if (table.getSelectedRowCount() == 0) {
				JOptionPane.showMessageDialog(null, " Selecione um Cliente ", "", JOptionPane.INFORMATION_MESSAGE);
				// Se o numero de linhas selecionadas for = a 1
			} else if (table.getSelectedRowCount() == 1) {
				int x;
				// Pega a linha selecionada na tabela e armazena em x
				x = table.getSelectedRow();
				// Pega o ID da linha x da tabela e armazena em y
				int y = (int) table.getValueAt(x, 0);
				// Abre a janela editar passando como parametro o ID armazenado
				// em y e fecha a janela atual
				new JanelaEditar(y);
				this.dispose();
				// Caso contrario
			} else {
				// retorna msg
				JOptionPane.showMessageDialog(null, " Selecione Apenas Um Cliente ", "",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}
