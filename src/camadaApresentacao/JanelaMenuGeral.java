package camadaApresentacao;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class JanelaMenuGeral extends JFrame implements ActionListener {
	private JButton buttonCadastrar, buttonConsultar, buttonRelatorioGeral, buttonExit;
	private JLabel labelTitulo;

	public JanelaMenuGeral() {
		super("Menu Principal");
		Container content = getContentPane();
		content.setLayout(null);

		Font font = new Font("Serif", Font.PLAIN, 12);
		content.setFont(font);

		String labelText = "Bem-vindo";
		labelTitulo = new JLabel(labelText);
		labelTitulo.setBounds(new Rectangle(175, 40, 200, 40));
		content.add(labelTitulo);

		// botao cadastrar
		buttonCadastrar = new JButton("Cadastrar Novo Cliente");
		buttonCadastrar.setBounds(110, 100, 200, 40);
		content.add(buttonCadastrar);
		buttonCadastrar.setActionCommand("CADASTRAR");
		buttonCadastrar.addActionListener(this);

		// botao consultar
		buttonConsultar = new JButton("Editar Clientes");
		buttonConsultar.setBounds(110, 200, 200, 40);
		content.add(buttonConsultar);
		buttonConsultar.setActionCommand("CONSULTAR");
		buttonConsultar.addActionListener(this);

		// botao relatorio geral
		buttonRelatorioGeral = new JButton("Relatorio Geral");
		buttonRelatorioGeral.setBounds(new Rectangle(110, 300, 200, 40));
		content.add(buttonRelatorioGeral, null);
		buttonRelatorioGeral.setActionCommand("RELATORIO");
		buttonRelatorioGeral.addActionListener(this);

		// Botao Sair
		buttonExit = new JButton("Sair");
		buttonExit.setBounds(new Rectangle(110, 400, 200, 40));
		content.add(buttonExit, null);
		buttonExit.setActionCommand("EXIT");
		buttonExit.addActionListener(this);

		setVisible(true);
		setSize(420, 500);
		setResizable(false);
	}

	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		if (comando.equals("RELATORIO")) {
			// Abre uma nova janela do tipo relatorio geral
			new JanelaRelatorioGeral();
			this.dispose();
		}
		if (comando.equals("CADASTRAR")) {
			// Abre uma nova janela do tipo cadastrar novo cliente
			new JanelaCadastroCliente();
			this.dispose();
		}
		if (comando.equals("CONSULTAR")) {
			// Abre uma nova janela do tipo editar e consultar clientes
			new JanelaConsulta();
			this.dispose();
		}
		if (comando.equals("EXIT")) {
			// Sai do sistema
			System.exit(EXIT_ON_CLOSE);
		}
	}
}
