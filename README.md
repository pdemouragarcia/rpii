## GRUPO E ##

Um cadastro de clientes válido e completo pode
ser útil para uma empresa conhecer o perfil de seu
cliente, para utilizar ações de marketing visando
otimizar as vendas ou até mesmo uma necessidade
legal.